package liwey.keystat.service;

import liwey.keystat.dao.KeystatDao;
import liwey.keystat.dao.KeystatDaoImpl;
import liwey.keystat.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static liwey.keystat.service.GsonUtil.getMapType;
import static liwey.keystat.service.GsonUtil.gson;

@RestController
@RequestMapping(value = "/ks")
public class KeystatController {
    private KeystatDao dao;

    public KeystatController() {
        ApplicationContext context = new ClassPathXmlApplicationContext("db.xml");
        dao = (KeystatDaoImpl) context.getBean("dao");
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String getHome() throws Exception {
        return "OK";
    }

    @RequestMapping(value = "/status", method = RequestMethod.GET)
    public String getStatus() throws Exception {
        return "OK";
    }

    /// users
    @RequestMapping(value = "/uid", method = RequestMethod.GET)
    public long newUid() throws Exception {
        return dao.maxUid() + 1;
    }

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public long getUserCount() throws Exception {
        long count = dao.getUserCount();
        return count;
    }

    @RequestMapping(value = "/users/{uid}", method = RequestMethod.GET)
    public User getUser(@PathVariable("uid") String uid) throws Exception {
        User user = dao.getUser(Integer.valueOf(uid));
        return user;
    }

    @RequestMapping(value = "/users", method = RequestMethod.POST)
    public long addUser(String json) throws Exception {
        User user = gson.fromJson(json, User.class);
        if(user.getId() == 0) {
            user.setId(dao.maxUid() + 1);
            dao.addUser(user);
        }else{
            dao.updateUser(user);
        }
        return user.getId();
    }

    @RequestMapping(value = "/trades", method = RequestMethod.GET)
    public List<Trade> getAllTrades() throws Exception {
        List<Trade> trades = dao.getAllTrades();
        return trades;
    }

    @RequestMapping(value = "/trades/{id}", method = RequestMethod.GET)
    public Trade getTrade(@PathVariable("id") String id) throws Exception {
        Trade trade = dao.getTrade(Integer.valueOf(id));
        return trade;
    }

    @RequestMapping(value = "/trades", method = RequestMethod.POST)
    public int addTrade(String json) throws Exception {
        Trade trade = gson.fromJson(json, Trade.class);
        int n = dao.addTrade2(trade);
        return n;
    }

    /// trades2
    @RequestMapping(value = "/trades/{pid}", method = RequestMethod.GET)
    public List<Trade2> getAllTrade2(@PathVariable("pid") String pid) throws Exception {
        List<Trade2> trades2 = pid == null ? dao.getAllTrade2() : dao.getAllTrade2(Integer.parseInt(pid));
        return trades2;
    }

    @RequestMapping(value = "/trades2/{pid}", method = RequestMethod.GET)
    public Trade2 getTrade2(@PathVariable("id") String id) throws Exception {
        Trade2 trade2 = dao.getTrade2(Integer.valueOf(id));
        return trade2;
    }

    @RequestMapping(value = "/trades2", method = RequestMethod.POST)
    public int addTrade2(String json) throws Exception {
        Trade2 trade2 = gson.fromJson(json, Trade2.class);
        int n = dao.addTrade2(trade2);
        return n;
    }

    // stats
    @RequestMapping(value = "/stats/{uid}", method = RequestMethod.GET)
    public List<Stat> getStats(@PathVariable("uid") String uid) throws Exception {
        List<Stat> stats = dao.getStats(Integer.valueOf(uid));
        return stats;
    }

    @RequestMapping(value = "/stats/{uid}", method = RequestMethod.PUT)
    public int updateStats(@PathVariable("uid") String uid, String json) throws Exception {
        HashMap<String, Integer> data = gson.fromJson(json, getMapType(String.class, Integer.class));
        List<Stat> stats = new ArrayList<>();
        for (String key : data.keySet()) {
            stats.add(new Stat(Integer.valueOf(uid), null, key, data.get(key)));
        }
        int n = dao.updateStats(stats);
        return n;
    }

    // remap
    @RequestMapping(value = "/remaps/{uid}", method = RequestMethod.GET)
    public List<Remap> getRemaps(@PathVariable("uid") String uid) throws Exception {
        List<Remap> remaps = dao.getRemaps(Integer.valueOf(uid));
        return remaps;
    }

    @RequestMapping(value = "/remaps/{uid}", method = RequestMethod.POST)
    public int addRemaps(@PathVariable("uid") String uid, String json) throws Exception {
        HashMap<String, String> data = gson.fromJson(json, getMapType(String.class, String.class));
        List<Remap> remaps = new ArrayList<>();
        for (String key : data.keySet()) {
            remaps.add(new Remap(Integer.valueOf(uid), key, data.get(key)));
        }
        int n = dao.updateRemaps(remaps);
        return n;
    }
}
