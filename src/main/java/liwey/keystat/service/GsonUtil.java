/*
 * Copyright
 *
 * @author      Leon Zeng
 * @version     1.0
 * Created       2018/4/10 19:38
 */

package liwey.keystat.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 */
public class GsonUtil {
    static Gson gson = new GsonBuilder().disableHtmlEscaping()
            .setPrettyPrinting().serializeNulls().create();

    static Type getArrayType(Class type) {
        return TypeToken.getParameterized(ArrayList.class, type).getType();
    }

    static Type getMapType(Class keyClass, Class valueClass) {
        return TypeToken.getParameterized(HashMap.class, keyClass, valueClass).getType();
    }
}
