package liwey.keystat.dao;

import liwey.keystat.model.Remap;
import liwey.keystat.model.Stat;
import liwey.keystat.model.Trade;
import liwey.keystat.model.Trade2;
import liwey.keystat.model.User;
import lombok.Setter;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.ArrayList;
import java.util.List;

@Log4j
public class KeystatDaoImpl implements KeystatDao {
    @Setter private JdbcTemplate jdbcTemplate;

    //user
    @Override
    public int maxUid() throws Exception {
        return jdbcTemplate.queryForObject("SELECT max(id) FROM users", Integer.class);
    }

    @Override
    public long getUserCount() throws Exception {
        return jdbcTemplate.queryForObject("SELECT count(*) FROM users", Long.class);
    }

    @Override
    public User getUser(int id) throws Exception {
        return jdbcTemplate.queryForObject("SELECT * FROM users WHERE id = ?", User.class, id);
    }

    @Override
    public int addUser(User user) throws Exception {
        String sql = "INSERT INTO users" +
                "(id, email,trade,trade2,ip,os,lang,timezone) " +
                "VALUES(?,?,?,?,?,?,?,?)";
        return jdbcTemplate.update(sql, user.getId(), user.getEmail(), user.getTrade(), user.getTrade2(), user.getIp(),
                user.getOs(), user.getLang(), user.getTimezone());
    }

    @Override
    public int updateUser(User user) throws Exception {
        String sql = "UPDATE users SET email=?,trade=?,trade2=?,ip=?," +
                "os=?,updated=CURRENT_TIMESTAMP,lang=?,timezone=? WHERE id=?";
        return jdbcTemplate.update(sql, user.getEmail(), user.getTrade(), user.getTrade2(), user.getIp(),
                user.getOs(), user.getLang(), user.getTimezone(), user.getId());
    }

    @Override
    public int deleteUser(int id) throws Exception {
        return jdbcTemplate.update("DELETE FROM users WHERE id=?", id);
    }


    // trade
    @Override
    public List<Trade> getAllTrades() throws Exception {
        return jdbcTemplate.queryForList("SELECT * FROM trades", Trade.class);
    }

    @Override
    public Trade getTrade(int id) throws Exception {
        return jdbcTemplate.queryForObject("SELECT * FROM trades WHERE id=?", Trade.class, id);
    }

    @Override
    public int addTrade2(Trade trade) throws Exception {
        String sql = "INSERT INTO trades (name) VALUES(?)";
        return jdbcTemplate.update(sql, trade.getName());
    }

//    public int updateTrade(Trade trade) throws Exception {
//        String sql = "update trades set name = ? where id =?";
//        return jdbcTemplate.insert(sql, trade.getName(),trade.getId());
//    }
//
//    public int deleteTrade(int id) throws Exception {
//        String sql = "delete trades where id =?";
//        return jdbcTemplate.update(sql, id);
//    }

    // trade2
    @Override
    public List<Trade2> getAllTrade2() throws Exception {
        return jdbcTemplate.queryForList("SELECT * FROM trades2", Trade2.class);
    }

    @Override
    public List<Trade2> getAllTrade2(int pid) throws Exception {
        return jdbcTemplate.queryForList("SELECT * FROM trades2 WHERE pid=? ORDER BY id", Trade2.class, pid);
    }

    @Override
    public Trade2 getTrade2(int id) throws Exception {
        return jdbcTemplate.queryForObject("SELECT * FROM trades2 WHERE id=?", Trade2.class, id);
    }

    @Override
    public int addTrade2(Trade2 trade2) throws Exception {
        String sql = "insert into trades2 (pid, name) values(?, ?)";
        return jdbcTemplate.update(sql, trade2.getPid(), trade2.getName());
    }

//    public int updateTrade2(Trade2 trade2) throws Exception {
//        String sql = "update trades2 set name = ? where id =?";
//        return jdbcTemplate.insert(sql, trade2.getName(),trade2.getId());
//    }
//
//    public int deleteTrade2(int id) throws Exception {
//        String sql = "delete trades2 where id =?";
//        return jdbcTemplate.update(sql, id);
//    }


    @Override
    public List<Stat> getStats(int uid) throws Exception {
        return jdbcTemplate.queryForList("SELECT * FROM stats WHERE uid=? ORDER BY ki,time", Stat.class, uid);
    }

    @Override
    public int updateStats(List<Stat> stats) throws Exception {
        if (stats.size() == 0)
            return 0;

        String sql = "INSERT INTO stats(uid,ki,count) VALUES(?,?,?)";
        List<Object[]> params = new ArrayList<>();
        for (Stat stat : stats) {
            params.add(new Object[]{stat.getUid(), stat.getKi(), stat.getCount()});
        }

        jdbcTemplate.update("DELETE FROM stats WHERE uid=?", stats.get(0).getUid());
        int[] result = jdbcTemplate.batchUpdate(sql, params);
        int sum = 0;
        for (int r : result)
            sum += r;
        return sum;
    }

    // remaps
    @Override
    public List<Remap> getRemaps(int uid) throws Exception {
        return jdbcTemplate.queryForList("SELECT * FROM remaps WHERE uid=? ORDER BY ki1", Remap.class, uid);
    }

    @Override
    public int updateRemaps(List<Remap> remaps) throws Exception {
        String sql = "insert into remaps(uid, ki1, ki2) values(?,?,?)";
        List<Object[]> params = new ArrayList<>();
        for (Remap remap : remaps) {
            params.add(new Object[]{remap.getUid(), remap.getKi1(), remap.getKi2()});
        }
        jdbcTemplate.update("DELETE FROM remaps WHERE uid=?", remaps.get(0).getUid());
        int[] result = jdbcTemplate.batchUpdate(sql, params);
        int sum = 0;
        for (int r : result)
            sum += r;
        return sum;
    }
}


