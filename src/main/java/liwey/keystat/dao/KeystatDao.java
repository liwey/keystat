package liwey.keystat.dao;

import liwey.keystat.model.Remap;
import liwey.keystat.model.Stat;
import liwey.keystat.model.Trade;
import liwey.keystat.model.Trade2;
import liwey.keystat.model.User;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 *
 */
@Repository
public interface KeystatDao {
    //user
    int maxUid() throws Exception;

    long getUserCount() throws Exception;

    User getUser(int id) throws Exception;

    int addUser(User user) throws Exception;

    int updateUser(User user) throws Exception;

    int deleteUser(int id) throws Exception;

    // trade
    List<Trade> getAllTrades() throws Exception;

    Trade getTrade(int id) throws Exception;

    int addTrade2(Trade trade) throws Exception;

    // trade2
    List<Trade2> getAllTrade2() throws Exception;

    List<Trade2> getAllTrade2(int pid) throws Exception;

    Trade2 getTrade2(int id) throws Exception;

    int addTrade2(Trade2 trade2) throws Exception;

    List<Stat> getStats(int uid) throws Exception;

    int updateStats(List<Stat> stats) throws Exception;

    // remaps
    List<Remap> getRemaps(int uid) throws Exception;

    int updateRemaps(List<Remap> remaps) throws Exception;
}
