package liwey.keystat.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Remap {
    private long uid;
    private String ki1;
    private String ki2;
}
