package liwey.keystat.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Stat {
    private long uid;
    private Timestamp time;
    private String ki;
    private int count;
}
