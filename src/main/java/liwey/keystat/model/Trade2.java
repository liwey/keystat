package liwey.keystat.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Trade2 {
    private long id;
    private long pid;
    private String name;
}
