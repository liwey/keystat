package liwey.keystat.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class User {
    private long id;
    private String email;
    private long trade;
    private long trade2;
    private String ip;
    private String os;
    private long created;
    private long updated;
    private String lang;
    private int timezone;
}
